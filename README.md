# MTG Telegram Bot

mtg-telegram-bot is a chatbot for Magic the Gathering fans who used [Telegram](https://telegram.org/).
The project is based on [python-telegram-bot](https://python-telegram-bot.org/), a telegram API wrapper.

## Features
* Surround your card names by "*" and get images directly in the chat (\*my card\*)
* An inline query search engine to easily send cards on the chat.
* Construct [Cockatrice](https://cockatrice.github.io/) xml files for testing new sets at any time.
* 3 games at the moment including a pictionnary, card quiz and hangman.

This bot always has access to the latest cards thanks to the [Scryfall Database API](https://scryfall.com/docs/api).

## Install
### Prerequisite
* Docker and docker-compose installed on your machine
* A Telegram account with an [API Token](https://core.telegram.org/bots#creating-a-new-bot)

### Deployement
1. Clone the repository
```
git clone https://gitlab.com/nicolas_capon/mtg_telegram_bot.git
cd mtg_telegram_bot/
```
2. In /app, Rename config.py.ex to config.py then fill the file with your credentials
   (API token, your telegram user_id and the chat_id of your group_chat)
3. Build the docker image and run a container
```
sudo docker-compose up --build mtg_telegram_bot
```
4. Start a conversation with the bot and send /start to see if it works.

Don't forget to bring the bot in your group chat to enjoy the games with your friends

## TODO:
* Implement get_trophy method
