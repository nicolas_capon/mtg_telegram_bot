from typing import List
from telegram.ext import Dispatcher, CommandHandler
from telegram.ext.handler import Handler


class Service:

    def __init__(self, dispatcher: Dispatcher):
        self.dispatcher = dispatcher
        self.handlers: List[Handler] = []
        self.helplist: List[(CommandHandler, str)] = []

    def add_handler(self, handler: Handler, help_text="", arguments=List[str]) -> None:
        """
        Add handler to the handler list
        :param handler: Handler PTB Object
        :param help_text: str help for CommandHandler only
        :param arguments: List[str] list of arguments for the comand (for user doc)
        :return: None
        """
        if isinstance(handler, CommandHandler):
            self.helplist.append((handler, help_text, arguments))
        self.handlers.append(handler)

    def remove_handler(self, handler: Handler) -> None:
        """
        Remove handler to the handler list
        :param handler: Handler PTB Object
        :return: None
        """
        self.handlers.remove(handler)

    def start(self) -> None:
        """
        Start each handlers
        :return: None
        """
        for handler in self.handlers:
            self.dispatcher.add_handler(handler)

    def stop(self) -> None:
        """
        Stop each handlers
        :return:
        """
        for handler in self.handlers:
            self.dispatcher.remove_handler(handler)

    def get_help(self) -> str:
        """
        Get Service help for end user.
        :return: str help
        """
        text = ""
        for handler, help_text, arguments in self.helplist:
            for command in handler.command:
                if handler.pass_args:
                    arg_text = ", ".join(arguments)
                    text += f"/{command} [{arg_text}] - {help_text}\n"
                else:
                    text += f"/{command} - {help_text}\n"
        return text
