import re
import os
from tools import get_user_tag
from typing import List
from config import app_dir
from datetime import datetime, timedelta
from scryfall_api import Scryfall, Card
from sqlalchemy import Column, Integer, String, DateTime, create_engine, Enum as SQLEnum
from sqlalchemy.orm import sessionmaker, scoped_session, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import ForeignKey
from telegram import Update, Message
from telegram.ext import CallbackContext
from enum import Enum, auto

engine = create_engine(f"sqlite:///{os.path.join(app_dir, 'db', 'games.db')}",
                       connect_args={'check_same_thread': False})
Base = declarative_base()


class GameStates(Enum):
    INIT = auto()
    PLAY = auto()
    END = auto()


class Winner(Base):

    __tablename__ = "winner"

    game_id = Column(Integer, ForeignKey("game.uid"), primary_key=True)
    user_id = Column(Integer, primary_key=True)

    # user = relationship("User")
    game = relationship("TelegramQuizGame")

    def __repr__(self):
        return f"<Winner(game_id={self.game_id}, user_id={self.user_id})>"


class TelegramQuizGame(Base):

    description = "Quizz game"

    __tablename__ = "game"
    game_name = Column("name", String(32), nullable=False)
    uid = Column(Integer, primary_key=True)
    message_id = Column(Integer)
    chat_id = Column(Integer)
    object_id = Column(String)
    starter_id = Column(Integer)
    state = Column(SQLEnum(GameStates))
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    answer = Column(Integer)

    __mapper_args__ = {
        'polymorphic_identity': 'game',
        'polymorphic_on': game_name
    }

    winners = relationship(Winner)

    def __init__(self):
        self.state = GameStates.INIT
        self.quiz = ""
        self.end_message = ""
        self.quiz_object = None
        Session.add(self)

    def get_winners(self) -> List[Winner]:
        return self.winners

    def set_winners(self, winners: List[int]) -> None:
        for winner in winners:
            self.winners.append(Winner(game_id=self.uid, user_id=winner))

    def get_duration(self) -> timedelta:
        """
        Compute game length
        :return: timedelta length of the game
        """
        if self.end_time:
            return self.end_time - self.start_time
        else:
            return datetime.now() - self.start_time

    def start(self, starter_id=None) -> None:
        self.start_time = datetime.now()
        self.starter_id = starter_id
        self.state = GameStates.PLAY
        Session.commit()

    def stop(self, winners: List[int] = None) -> None:
        """
        Stop the current game and save its state. If no winners provided, the game is a draw
        :param winners: List[User.id]
        :return: None
        """
        self.end_time = datetime.now()
        self.set_winners(winners)
        self.state = GameStates.END
        Session.commit()

    def cancel(self):
        Session.remove(self)

    def send_quiz(self, update: Update, context: CallbackContext) -> bool:
        """
        Send Quizz message to the chat, in case of problem return False
        :param update: Update PTB object
        :param context: CallbackContext PTB object
        :return: bool True if all was ok, False, otherwise
        """
        self.chat_id = update.message.chat_id
        message = update.message.reply_text(text=self.quiz, quote=True, disable_web_page_preview=False)
        self.message_id = message.message_id
        return True

    def get_trophy(self):
        # TODO: implement
        pass

    def is_good_answer(self, message: Message) -> bool:
        is_correct = self.answer in message.text.lower()
        if is_correct:
            self.stop(winners=[message.from_user.id])
        return is_correct

    def get_answer_url(self):
        return self.quiz_object.get_main_image_url()

    @classmethod
    def get_random_card(cls, q: str = "", max_try=5):
        """
        Return a random card that have an art_crop image available
        :param q: str query for Scryfall API to filter random result
        :param max_try: int max number of attempts at finding a valid random card
        :return: Card if found None otherwise
        """
        card = Scryfall().get_random_card(q=q)
        if card.get_main_image_url(size="art_crop"):
            return card
        elif max_try:
            return cls.get_random_card(q=q, max_try=max_try-1)
        else:
            raise Exception("Too much attempts at finding a valid card.")


class ArtGame(TelegramQuizGame):

    name = "art_game"
    description = "Art quizz game"
    __mapper_args__ = {'polymorphic_identity': name}

    def __init__(self):
        super().__init__()
        self.quiz_object = self.get_random_card(q="-t:basic")
        self.object_id = self.quiz_object.id
        self.answer = self.quiz_object.name.lower().split("//")[0] # Split for double faced cards
        art = self.quiz_object.get_main_image_url(size="art_crop")
        self.quiz = f"Devine le nom de <a href='{art}'>cette carte</a> pour gagner la manche !"


class DrawGame(TelegramQuizGame):

    name = "draw_game"
    description = "Pictionnary style game"
    __mapper_args__ = {'polymorphic_identity': name}

    def __init__(self):
        super().__init__()
        self.quiz_object: Card = self.get_random_card(q="t:legendary")
        self.object_id = self.quiz_object.id
        self.answer = self.quiz_object.name.lower().split("//")[0] # Split for double faced cards
        self.quiz = f"Fait deviner le nom de <a href='{self.quiz_object.get_main_image_url()}'>cette carte</a> " \
                    f"en dessinant sur la feuille ci-desous puis en l'envoyant dans le chat."

    def send_quiz(self, update: Update, context: CallbackContext) -> bool:
        self.chat_id = update.message.chat_id
        user = get_user_tag(update.message.from_user)
        text = f"{user} vous prépare un dessin de qualité !\n" \
               f"A vous de deviner la carte qui se cache derrière l'image qu'il va vous envoyer..."
        message = update.message.reply_text(text=text, quote=True, disable_web_page_preview=False)
        self.message_id = message.message_id
        try:
            message = update.message.from_user.send_message(text=self.quiz, disable_web_page_preview=False)
            # Send white image to drawn on
            message.reply_photo("https://www.drodd.com/images14/white3.jpg")
        except Exception as e:
            # We cant send private message to user if he doesnt already have a private chat with the bot
            text = f"{user}, je ne peux pas t'envoyer de message privé pour le moment, " \
                   f"démarre un chat privé avec moi en cliquant ici: {get_user_tag(context.bot.get_me())}"
            update.message.reply_text(text=text, quote=True, disable_web_page_preview=False)
            return False
        return True

    def is_good_answer(self, message: Message) -> bool:
        is_correct = self.answer in message.text.lower() and message.from_user.id != self.starter_id
        if is_correct:
            self.stop(winners=[self.starter_id, message.from_user.id])
        return is_correct


class WordGame(TelegramQuizGame):

    name = "word_game"
    description = "Hangman style game"
    __mapper_args__ = {'polymorphic_identity': name}

    def __init__(self):
        super().__init__()
        self.quiz_object = self.get_random_card(q="-t:basic")
        self.object_id = self.quiz_object.id
        self.answer = self.quiz_object.name.lower().split("//")[0] # Split for double faced cards
        self.current_guess = re.sub(r"[a-z]", "˽", self.answer)
        self.max_tries = 6
        self.tries = []
        self.players: List[int] = []
        self.quiz = f"Devine le nom de la carte ci-dessous pour gagner la manche !\n{self.current_guess}"

    def is_good_answer(self, message: Message) -> bool:
        # Search a string containing exactly one letter
        result = re.search(r"^[a-zA-Z]$", message.text)
        if result and result[0].lower() in self.answer:
            letter = result[0].lower()
            # Good answer
            if result[0].lower() in self.tries:
                # letter already tried
                return False
            self.tries.append(letter)
            if message.from_user.id not in self.players:
                self.players.append(message.from_user.id)
            self.current_guess = "".join(
                [letter if letter == self.answer[i] else char for i, char in enumerate(self.current_guess)]
            )
            # Game is over
            if self.current_guess == self.answer:
                self.stop(winners=self.players)
                return True
            tries_string = ",".join(self.tries)
            message.reply_text(text=f"{self.max_tries} essai(s) restant(s).\n{self.current_guess}\n[{tries_string}]")
        elif result and self.max_tries:
            # Wrong answer
            if result[0].lower() not in self.tries:
                self.tries.append(result[0].lower())
            tries_string = ",".join(self.tries)
            self.max_tries -= 1
            message.reply_text(text=f"{self.max_tries} essai(s) restant(s).\n{self.current_guess}\n[{tries_string}]")
        elif result:
            # Wrong answer and no more tries left
            self.stop()
            return True
        return False


def get_winners() -> List[int]:
    """
    Get List of all unique winners id
    :return: List[int] of all unique winners_id
    """
    return [winner.user_id for winner in Session.query(Winner.user_id).distinct(Winner.user_id)]


def get_win_count(user_id: int, game: TelegramQuizGame = None) -> int:
    """
    Get number of win for a specific user and game. If no game provided, send total wins
    :param user_id: User.id
    :param game: TelegramQuizGame Object
    :return: int Number of wins
    """
    if game:
        return Session.query(Winner.user_id).join(TelegramQuizGame)\
            .filter(Winner.user_id == user_id, TelegramQuizGame.game_name == game.name).count()
    else:
        return Session.query(Winner.user_id).filter(Winner.user_id == user_id).count()


Base.metadata.create_all(engine)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

if __name__ == "__main__":
    print(get_win_count(257145716))
