import json
import requests
import logging
from time import sleep
from urllib.parse import quote_plus
from functools import wraps


class Card:

    def get_main_image_url(self, size="normal"):
        """
        Return front image of the card
        :param size: size of the image to be returned
        :return: str URL of the image
        """
        if getattr(self, "image_uris", None):
            return self.image_uris.get(size)
        elif getattr(self, "card_faces", None):
            return self.card_faces[0].get("image_uris", {}).get(size, None)

    def get_images_url(self, size="normal"):
        """
        Get list of image of given size for the card
        :param size: size of images to be returned
        :return: list of images if card have multiple faces else return list of single image
        """
        images = []
        if getattr(self, "image_uris", None):
            images.append(self.image_uris.get(size))
        elif getattr(self, "card_faces", None):
            images = [f["image_uris"][size] for f in self.card_faces if f.get("image_uris", {}).get(size, None)]
        return images

    def get_url(self) -> str:
        """
        Get scryfall url of the card
        :return: str URL of the card
        """
        if getattr(self, "scryfall_uri", None):
            return self.scryfall_uri

    def get_set(self):
        """
        Get Set Object from card
        :return: Set Scryfall Object
        """
        if getattr(self, "set", None):
            return Scryfall().get_set(self.set)

    def get_price(self, currency="eur", foil=False):
        if foil:
            currency += "_foil"
        return getattr(self, "prices", {}).get(currency)

    def get_purchase_uri(self, site="cardmarket"):
        return getattr(self, "purchase_uris", {}).get(site)

    def __repr__(self):
        return f"<Card(name={self.name}, set={self.set}, url={self.scryfall_uri})>"


class Expansion:

    def get_cards(self) -> [Card]:
        """
        Get card list for this expansion, if not already set, fetch the cards and set them as cards attribute
        :return: list of Card objects
        """
        if getattr(self, "cards", None):
            return self.cards
        elif getattr(self, "search_uri", None):
            cards = Scryfall.object_factory(**Scryfall.get_content(self.search_uri))
            setattr(self, "cards", cards)
            return cards

    def __repr__(self):
        return f"<Expansion(name={self.name}, url={self.scryfall_uri})>"


def endpoint(route):
    def inner_function(function):
        @wraps(function)
        def wrapped(*args, **kwargs):
            params = function(*args, **kwargs)
            if route.endswith("/"):
                query = route + params
            else:
                query = f"{route}?{params}"
            content = Scryfall.get_content(Scryfall.domain + query, merge_pages="page=" not in query)
            return Scryfall.object_factory(**content)

        return wrapped

    return inner_function


class Scryfall:
    """
    Scryfall class
    To create endpoints, use decorator endpoint with corresponding route (like: /sets)
    and return end of url (like /aer to return Aether Revolt expansion object)
    """

    domain = "https://api.scryfall.com"
    spam_limit = 0.1

    @classmethod
    def get_content(cls, query: str, merge_pages: bool = True):
        """
        Extract data from API JSON file. If there is multiple pages, gather them in one list.
        :param query: str query for API domain or complete url to request
        :param merge_pages: bool if True include all pages to
        :return: dict corresponding to API JSON response
        """
        if not query:
            return False
        sleep(cls.spam_limit)  # Time limit of scryfall API
        r = requests.get(query)
        data = {}
        if r.status_code == requests.codes.ok:
            data = json.loads(r.content.decode('utf-8'))
            if data.get("object", False) == "error":
                logging.getLogger("bot_logger").info(f"API respond an error to query : {query}")
                return None
            elif merge_pages and data.get("has_more", None) and data.get("next_page", None):
                content = cls.get_content(data["next_page"])
                data["data"] += content.get("data", [])
        return data

    @staticmethod
    def query_formatter(key_parameters, **kwargs):
        """
        Create end of query url with parameters.
        :param key_parameters: dict of url parameters like sorting, filtering... (not corresponding to the search itself)
        :param kwargs: dict of all url parameters to be considered
        :return: str Tail of the url formatted to scryfall API
        """
        # Extract key parameters from regular ones in **kwargs
        params = []
        for key, value in key_parameters.items():
            if kwargs.get(key) and isinstance(kwargs.get(key), value):
                params.append(f"{key}={kwargs.pop(key)}")
        params = "&".join(params)

        # Test if remaining parameters contains q, if so query is ready
        q = kwargs.get("q")
        if q and params:
            return f"{params}&q={quote_plus(q)}"
        elif q:
            return f"q={quote_plus(q)}"
        else:
            # q not in parameters then create it manually
            query = []
            for key, value in kwargs.items():
                if isinstance(value, str):
                    query.append(quote_plus(f"{key}:{value}"))
                else:
                    query.append(quote_plus(f"{key}={value}"))
            return f"{params}" + "+".join(query)

    @classmethod
    def object_factory(cls, **attributes):
        """
        Create object dynamically according to api JSON.
        :param attributes: dict from API JSON response
        :return: Object corresponding to API response
        """
        if attributes.get("object") == "list" and attributes.get("data"):
            return [cls.object_factory(**o) for o in attributes.get("data")]
        elif attributes.get("object") == "set":
            return type(attributes.get("object"), (Expansion,), attributes)()
        elif attributes.get("object") == "card":
            return type(attributes.get("object"), (Card,), attributes)()
        elif attributes.get("object"):
            return type(attributes.get("object"), (object,), attributes)()

    @endpoint("/sets/")
    def get_set(self, code: str):
        """
        Get Expansion by its code.
        :param code: three or four letter code of the Expansion
        :return: Expansion
        """
        return code.lower()

    @endpoint("/cards/random")
    def get_random_card(self, **kwargs):
        query = None
        if kwargs:
            key_parameters = {"format": str,
                              "face": str,
                              "version": str,
                              "pretty": bool}
            query = self.query_formatter(key_parameters, **kwargs)
        return query

    @endpoint("/cards/named")
    def get_card(self, **kwargs):
        """
        https://scryfall.com/docs/api/cards/named
        exact or fuzzy aren't optional
        :param name: str name of the card to search
        :param kwargs: dict of parameters to filter the search
        :return: Card object if found None otherwise
        """
        query = None
        if kwargs:
            key_parameters = {"exact": str,
                              "fuzzy": str,
                              "set": str,
                              "face": str,
                              "version": str,
                              "format": str,
                              "pretty": bool}
            query = self.query_formatter(key_parameters, **kwargs)
        return query

    @endpoint("/cards/search")
    def search_cards(self, **kwargs):
        """
        Search for a Card with given parameters.
        :param kwargs: dict containing parameters to search for
        :return: list of Card objects
        """
        if kwargs:
            key_parameters = {"unique": str,
                              "order": str,
                              "dir": str,
                              "include_extras": bool,
                              "include_multilingual": bool,
                              "include_variations": bool,
                              "page": int,
                              "format": str,
                              "pretty": bool}
            return self.query_formatter(key_parameters, **kwargs)

    @endpoint("/cards/autocomplete")
    def autocomplete(self, name: str):
        """
        Get a catalog of card name based on incomplete card name search
        :param name: str fuzzy card name
        :return: Catalog Scryfall object
        """
        return self.query_formatter({"q": name})


if __name__ == "__main__":
    s = Scryfall()
    print(s.get_random_card(q="-t:basic"))
