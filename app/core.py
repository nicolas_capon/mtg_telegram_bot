import re
from scryfall_api import Scryfall
from model import Service
from tools import send_cards_photos, card_to_inline_article, get_user_tag
from telegram import ChatAction, Message
from telegram.ext import MessageHandler, Filters, MessageFilter, InlineQueryHandler


class CoreHandler(Service):
    """
    This class handle the core fucntions of the bot:
        - Send card images when a message contains a card between *
        - Inline card search to send image
    """

    def __init__(self, dispatcher):
        super().__init__(dispatcher)
        self.card_regex = re.compile(r'\*(.*?)\*')
        self.set_regex = re.compile(r'\[([a-zA-Z0-9]{3,4})\]')
        card_image_filter = ImageMessageFilter(regex=self.card_regex)

        # Handlers
        self.add_handler(MessageHandler(Filters.text & card_image_filter, self.card_finder))
        self.add_handler(InlineQueryHandler(self.card_inline_query))

    def card_finder(self, update, context):
        """
        Find card in between * and send them in chat or send a message if not found
        :param update: Update PTB Object
        :param context: Context PTB Object
        :return: None
        """
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.UPLOAD_PHOTO)
        message = update.effective_message
        cardnames = self.card_regex.findall(message.text)
        cards, not_found = [], []
        for cardname in cardnames:
            set_code = self.set_regex.findall(cardname)
            if len(self.set_regex.findall(cardname)) == 1:
                card = Scryfall().get_card(fuzzy=cardname, set=set_code[0])
            else:
                card = Scryfall().get_card(fuzzy=cardname)
            if card:
                cards.append(card)
            else:
                not_found.append(cardname)

        if cards:
            send_cards_photos(cards,
                              context.bot,
                              chat_id=message.chat.id,
                              disable_notification=True,
                              reply_to_message_id=message.message_id,
                              allow_sending_without_reply=True)
        # If cards were not found, send a informative message
        if not_found:
            text = f"Désolé {get_user_tag(message.from_user)}, "\
                   "je ne trouve pas l'illustration de la ou les carte(s) suivante(s):\n"
            for cardname in not_found:
                text += "- " + cardname + "\n"
            sent = context.bot.send_message(chat_id=message.chat.id, text=text)
            context.job_queue.run_once(lambda x: context.bot.delete_message(sent.chat.id, sent.message_id),
                                       when=15)

    @staticmethod
    def card_inline_query(update, context):
        """Handle the inline query."""
        max_result = 50
        query = update.inline_query.query
        if not query.endswith("."):
            update.inline_query.answer([])
            return
        else:
            query = query[:-1]
        offset = update.inline_query.offset
        if not offset:
            page = 1
            result = 0
        else:
            page, result = offset.split("-")
            result = int(result)
        search = Scryfall().search_cards(q=query, page=int(page))
        if search and len(search) >= result + max_result:
            results = [card_to_inline_article(card) for card in search[result: result + max_result]]
            result += max_result
            update.inline_query.answer(results, next_offset=f"{page}-{result}")
        elif search:
            results = [card_to_inline_article(card) for card in search[result:-1]]
            update.inline_query.answer(results, next_offset=f"{int(page) + 1}-0")
        else:
            update.inline_query.answer([])

    def get_help(self) -> str:
        text = "Type @GeekStreamBot and a <a href='https://scryfall.com/docs/syntax'>scryfall card search</a> " \
               "followed by a dot to search for a card to send in the chat.\nEx: @GeekStreamBot snapcaster lang:fr.\n"
        return text + super().get_help()


class ImageMessageFilter(MessageFilter):
    """Filter use to detect if message contain a card to display"""
    def __init__(self, regex: re.Pattern):
        self.regex = regex

    def filter(self, message: Message) -> bool:
        """
        Filter message matching the regex
        :param message: message to parse
        :return: True if message contains the expression, False if not
        """
        return isinstance(self.regex.search(message.text), re.Match)
