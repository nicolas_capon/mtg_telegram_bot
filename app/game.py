import config
from typing import Set
from tools import get_user_tag, chat_restricted
from model import Service
from game_model import TelegramQuizGame, ArtGame, DrawGame, WordGame, get_winners, get_win_count
from telegram import ParseMode, Message, Update, ChatAction
from telegram.ext import CommandHandler, MessageHandler, MessageFilter, Filters, CallbackContext
from functools import partial


class GameHandler(Service):

    rank: Set[int] = {0, 1, 2}

    def __init__(self, dispatcher):
        super().__init__(dispatcher)
        self.games = [WordGame, DrawGame, ArtGame]
        # Handlers
        for game in self.games:
            self.add_handler(CommandHandler(game.name, partial(self.start_game, game=game)),
                             help_text=game.description)
        self.add_handler(CommandHandler("scores", self.get_scores), help_text="get games scores")

    @chat_restricted(config.chat_id)
    def start_game(self, update: Update, context: CallbackContext, game: TelegramQuizGame):
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        current_game = getattr(self, game.name, None)
        if not current_game:
            current_game = game()
            setattr(self, game.name, current_game)
            handler = MessageHandler(filters=Filters.text & GameFilter(current_game),
                                     callback=lambda x, y: self.send_congrats(x, y, game=game))
            current_game.start(starter_id=update.message.from_user.id)
            setattr(current_game, "handler", handler)
            self.dispatcher.add_handler(handler)
            game_ok = current_game.send_quiz(update, context)
            if not game_ok:
                current_game.cancel()
                setattr(self, game.name, None)
        else:
            context.bot.send_message(text="Une partie est déjà lancée...",
                                     chat_id=update.message.chat_id,
                                     reply_to_message_id=current_game.message_id)

    def send_congrats(self, update: Update, context: CallbackContext, game: TelegramQuizGame):
        current_game: TelegramQuizGame = getattr(self, game.name, None)
        # Remove the specific message handler to avoid sending congrats multiple times
        self.dispatcher.remove_handler(current_game.handler)
        winners = current_game.get_winners()
        if winners:
            text = f"Bravo ! <a href='{current_game.get_answer_url()}'>\U0001F5BC</a> " \
                   f"<a href='{current_game.get_trophy()}'>\U0001F3C6</a>\nLes gagnants sont:\n"
            for winner in winners:
                text += f"- {get_user_tag(update.effective_chat.get_member(user_id=winner.user_id).user)}\n"
        else:
            text = f"Vous avez échoués ! <a href='{current_game.get_trophy()}'>\U0001F4A9</a>"
        update.message.reply_text(text=text, parse_mode=ParseMode.HTML)
        setattr(self, game.name, None)  # Reset game atributes

    @chat_restricted(config.chat_id)
    def get_scores(self, update: Update, context: CallbackContext):
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        chat = update.effective_chat
        text = "<u> Games Leader Board:</u>\n\n- <u>Total wins:</u>\n"
        leader_board = sorted([(user_id, get_win_count(user_id)) for user_id in get_winners()],
                              key=lambda x: x[1],
                              reverse=True)
        for rank, (user, win_count) in enumerate(leader_board):
            text += f"    · {get_user_tag(chat.get_member(user).user)}: {win_count} {self.get_medal(rank)}\n"
        for game in self.games:
            text += f"\n- <u>{game.name} wins:</u>\n"
            leader_board = sorted([(user_id, get_win_count(user_id, game=game)) for user_id in get_winners()],
                                  key=lambda x: x[1],
                                  reverse=True)
            for rank, (user, win_count) in enumerate(leader_board):
                text += f"    · {get_user_tag(chat.get_member(user).user)}: {win_count} {self.get_medal(rank)}\n"
        update.effective_message.reply_text(text=text)

    @staticmethod
    def get_medal(rank: rank) -> str:
        """
        Get the medal emoji unicode for selected rank between 0 and 2
        :param rank: podium rank
        :return: str unicode emoji
        """
        medals = ["\U0001F947", "\U0001F948", "\U0001F949"]
        if rank < len(medals):
            return medals[rank]
        else:
            return ""


class GameFilter(MessageFilter):

    def __init__(self, game: TelegramQuizGame):
        self.game = game

    def filter(self, message: Message) -> bool:
        """
        Filter message matching the good answer and set game state accordingly
        :param message: Message to parse
        :return: True if message contains the expression, False if not
        """
        return self.game.is_good_answer(message)
