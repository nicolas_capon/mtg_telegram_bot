import logging
from telegram import InputMediaPhoto, InlineQueryResultPhoto, ParseMode, Bot, User, Update
from telegram.ext import CallbackContext
from scryfall_api import Card
from typing import List
from uuid import uuid4
from functools import wraps


def create_media_groups(medias, size=10) -> List:
    """
    Create media groups of the correct size (max = 10)
    :param medias: list of telegram InputMedia
    :param size: group size
    :return: list of InputMediaGroup ready to be sent
    """
    return [medias[i:i + size] for i in range(0, len(medias), size)]


def create_scryfall_photo(card: Card) -> List[InputMediaPhoto]:
    """
    Create photo ready to be sent from Scryfall Card object
    :param card: Card scryfall API object
    :return: list of InputMediaPhoto
    """
    return [InputMediaPhoto(media=url, caption=get_card_tag(card)) for url in card.get_images_url()]


def get_card_tag(card: Card, include_set=True, include_price=True) -> str:
    """
    Get HTML tag for message with html parsing
    :return: str html <a> tag
    """
    card_tag = f"<a href='{card.get_url()}'>{card.name}</a>"
    set_tag = f"<a href='{card.scryfall_set_uri}'> [{card.set.upper()}]</a>" if include_set else ""
    price = card.get_price()
    price_tag = f"<a href='{card.get_purchase_uri()}'> ({price}€)</a>" if include_price and price else ""
    return card_tag + set_tag + price_tag


def get_user_tag(user: User) -> str:
    return f"<a href='tg://user?id={user.id}'>{user.name}</a>"


def send_cards_photos(cards: list, bot: Bot, chat_id: int, **kwargs) -> None:
    """
    Send photos from a list of Cards
    :param cards: list of Card objects from scryfall API
    :param bot: Bot PTB object
    :param chat_id: int id of chat to send images
    :param kwargs: dict additional argument for sending the images like disable_notifications
    :return: None
    """
    photos = []
    for card in cards:
        photos += create_scryfall_photo(card)
    for album in create_media_groups(photos):
        if album:
            bot.send_media_group(chat_id=chat_id,
                                 media=album,
                                 disable_notification=kwargs.get("disable_notification", True),
                                 reply_to_message_id=kwargs.get("reply_to_message_id"),
                                 allow_sending_without_reply=kwargs.get("allow_sending_without_reply"))


def card_to_inline_article(card: Card) -> InlineQueryResultPhoto:
    """
    Prepare an InlineQueryResultArticle from a scryfall API Card object
    :param card:
    :return: InlineQueryResultPhoto
    """
    image = card.get_main_image_url()
    return InlineQueryResultPhoto(id=uuid4(),
                                  photo_url=image,
                                  thumb_url=image,
                                  photo_width=126,
                                  photo_height=176,
                                  title=card.name,
                                  caption=get_card_tag(card),
                                  parse_mode=ParseMode.HTML)


# def send_action(action):
#     """Sends `action` while processing func command."""
#
#     def decorator(func):
#         @wraps(func)
#         def command_func(update, context, *args, **kwargs):
#             context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
#             return func(update, context, *args, **kwargs)
#
#         return command_func
#
#     return decorator


def chat_restricted(chat_id):
    def decorator(func):
        @wraps(func)
        def wrapped(self, update: Update, context: CallbackContext, *args, **kwargs):
            if update.effective_chat.id != chat_id:
                logging.info("Unauthorized access denied for chat {}.".format(update.effective_chat))
                return
            return func(self, update, context, *args, **kwargs)
        return wrapped
    return decorator

#
# def restrict(user_type):
#     def decorator(func):
#         @wraps(func)
#         def command_func(update, context, *args, **kwargs):
#             users_id = None
#             if user_type is UserType.ADMIN:
#                 users_id = [id for id, in session.query(Player.id).filter(Player.is_admin == True).all()]
#             elif user_type is UserType.PLAYER:
#                 users_id = [id for id, in session.query(Player.id).all()]
#             if not update.effective_user.id in users_id:
#                 logging.info(f"Unauthorized access denied for {update.effective_user}.")
#                 return
#             return func(update, context, *args, **kwargs)
#
#         return command_func
#
#     return decorator