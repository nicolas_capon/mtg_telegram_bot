import config
import sys
import traceback
from typing import List
from model import Service
from game import GameHandler
from core import CoreHandler
from cockatrice import CockatriceHandler
from telegram import ParseMode, Update
from telegram.ext import Updater, Defaults, CommandHandler, CallbackContext
from telegram.utils.helpers import mention_html


class Bot:

    def __init__(self):
        """Initiate bot instance with all the functionalities"""
        self.git = "https://gitlab.com/nicolas_capon/mtg_telegram_bot"

        # Set up default behaviour
        defaults = Defaults(parse_mode=ParseMode.HTML)

        # Create the EventHandler and pass it your bot's token.
        updater = Updater(token=config.telegram_token, use_context=True, defaults=defaults)
        self.telegram_bot = updater.bot
        # Add features
        self.services: List[Service] = [CoreHandler(updater.dispatcher),
                                        CockatriceHandler(updater.dispatcher),
                                        GameHandler(updater.dispatcher)]
        for service in self.services:
            service.start()

        # Help handler
        updater.dispatcher.add_handler(CommandHandler(command="help",
                                                      callback=self.send_doc))

        # log all errors
        # updater.dispatcher.add_error_handler(self.error)

        # Start the Bot
        updater.start_polling()
        config.logger.info(f"{self.telegram_bot.get_me().name} started")
        # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        updater.idle()

    @staticmethod
    def error(update: Update, context: CallbackContext):
        # add all the dev user_ids in this list. You can also add ids of channels or groups.
        devs = [config.admin_id]
        if not update:
            for dev_id in devs:
                context.bot.send_message(dev_id,
                                         "".join(traceback.format_tb(sys.exc_info()[2])),
                                         parse_mode=ParseMode.HTML)
            raise
        # we want to notify the user of this problem. This will always work, but not notify users if the update is an
        # callback or inline query, or a poll update. In case you want this, keep in mind that sending the message
        # could fail
        if update.effective_message:
            text = "Hey. I'm sorry to inform you that an error happened while I tried to handle your update. " \
                   "My developer(s) will be notified."
            update.effective_message.reply_text(text)
        # This traceback is created with accessing the traceback object from the sys.exc_info, which is returned as the
        # third value of the returned tuple. Then we use the traceback.format_tb to get the traceback as a string, which
        # for a weird reason separates the line breaks in a list, but keeps the linebreaks itself. So just joining an
        # empty string works fine.
        trace = "".join(traceback.format_tb(sys.exc_info()[2]))
        # lets try to get as much information from the telegram update as possible
        payload = ""
        # normally, we always have an user. If not, its either a channel or a poll update.
        if update.effective_user:
            payload += f' with the user {mention_html(update.effective_user.id, update.effective_user.first_name)}'
        # there are more situations when you don't get a chat
        if update.effective_chat:
            payload += f' within the chat <i>{update.effective_chat.title}</i>'
            if update.effective_chat.username:
                payload += f' (@{update.effective_chat.username})'
        # but only one where you have an empty payload by now: A poll (buuuh)
        if update.poll:
            payload += f' with the poll id {update.poll.id}.'
        # lets put this in a "well" formatted text
        text = f"Hey.\n The error <code>{context.error}</code> happened{payload}. " \
               f"The full traceback:\n\n<code>{trace}</code>"
        # and send it to the dev(s)
        for dev_id in devs:
            context.bot.send_message(dev_id, text, parse_mode=ParseMode.HTML)
        # we raise the error again, so the logger module catches it. If you don't use the logger module, use it.
        raise

    def send_doc(self, update: Update, context):
        text = f"<u>{self.telegram_bot.get_me().name} documentation:</u>\n\n" \
               f"<u>General infos:</u>\nSource code and more about the project on <a href='{self.git}'>GitLab</a>\n\n" \
               f"<u>Chat Features:</u>\n"
        text += "".join([service.get_help() for service in self.services])
        update.effective_message.reply_text(text=text)
        pass

    def start(self, update: Update, context):
        text = f"Welcome {update.effective_user.name}, I'm {self.telegram_bot.get_me().name} " \
               f"you can find my infos here: /help"
        update.effective_message.reply_text(text=text)


if __name__ == '__main__':
    Bot()
