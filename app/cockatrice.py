from model import Service
from telegram.ext import CommandHandler
from cockatrice_api import get_cockatrice_file


class CockatriceHandler(Service):

    def __init__(self, dispatcher):
        super().__init__(dispatcher)

        self.add_handler(CommandHandler('cockatrice', self.send_xml, pass_args=True),
                         help_text="Get .cod file of mtg set",
                         arguments=["set_code"])

    @staticmethod
    def send_xml(update, context):
        arg_num = len(context.args)
        # Accepts only one arg for set_code
        if not arg_num == 1:
            message = f"Cette fonction ne prend que 1 argument ({arg_num} donné(s))"
            context.bot.sendMessage(chat_id=update.message.chat_id,
                                    text=message)
            return

        set_code = context.args[0]
        xml_file = get_cockatrice_file(set_code)
        if not xml_file:
            message = f"Cette édition est inconnue : [{set_code}]"
            context.bot.sendMessage(chat_id=update.message.chat_id,
                                    text=message)
            return

        context.bot.sendDocument(chat_id=update.message.chat_id, document=xml_file, filename=f"{set_code.lower()}.xml")
